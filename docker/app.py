from flask import Flask, jsonify
import boto3
import os
from botocore.exceptions import NoCredentialsError

app = Flask(__name__)

session = boto3.Session(
    aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
    aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
    aws_session_token=os.getenv('AWS_SESSION_TOKEN'),
)

def list_objects(bucket_name):
    try:
        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket_name)
        return [obj.key for obj in bucket.objects.all()]
    except NoCredentialsError:
        return []
    except Exception as e:
        print(f"Error: {str(e)}")
        return []


@app.route('/objects')
def objects():
    bucket_name = os.getenv('BUCKET_NAME')
    objects = list_objects(bucket_name)
    return jsonify(objects)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
