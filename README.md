# Logistics



## Getting started

Simple app that returns the s3 objects list

## Secrets in K8S

In Kubernetes, there are several ways to enhance the security of credential storage. One way is by integrating with external secrets management tools such as AWS Secrets Manager, Azure Key Vault, or HashiCorp Vault. These tools encrypt the data at rest and in transit, and provide fine-grained access control and audit logs. Another method is to use Kubernetes Secrets with Encryption at Rest, which encrypts secret data stored in etcd. Kubernetes also supports integration with KMS providers for key management. Lastly, using service accounts and RBAC can limit the exposure and permissions of secrets in the cluster.


## How to

```bash
docker build -t myapp:v1 -f docker/Dockerfile
k3d cluster create test \                                                                                                                                                                                                                                                   <aws:legacy-stage>
  --servers 3 \
  -p "8090:80@loadbalancer"
k3d image import myapp:v1 --cluster test
kubectl create secret generic aws-credentials --from-literal=AWS_ACCESS_KEY_ID=YourAccessKey --from-literal=AWS_SECRET_ACCESS_KEY=YourSecretKey --from-literal=AWS_SESSION_TOKEN=YourSessionToken
kubectl apply -f k8s/deploy.yaml -f k8s/svc.yaml -f k8s/ingress.yaml
echo "NODES-IP my-app.local.com" >> /etc/hosts

```